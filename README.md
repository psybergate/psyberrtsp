# rtsplayer
an rtsp player using VXG (Video Experts Group) SDK.


## Install

```cordova plugin add https://username@bitbucket.org/psybergate/psyberrtsp.git```

## Using

``` javascript
cordova.plugins.rtsplayer.watchVideo("rtsp://VIDEO_URI", callbackSucces, callbackError);
```

``` javascript
cordova.plugins.rtsplayer.watch("rtsp://VIDEO_URI", "user", "password" callbackSucces, callbackError);
```

## About
Available on Android and IOS
Uses VXG SDK

