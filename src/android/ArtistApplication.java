package za.co.psybergate.meme.rtsplayer;

import android.app.Application;
import android.content.Context;

public class ArtistApplication extends Application {
    private static ArtistApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
    }

    public static ArtistApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }
}
