package za.co.psybergate.meme.rtsplayer;

import android.net.Uri;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSyntaxException;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by ljjoubert on 2015/11/05.
 */

public class GsonRequest<T> extends JsonRequest<T> {

    // GsonBuilder parses dates/times in the locale timezone, so we need an adapter
    // to force use of UTC timezone.

    private static class UTCDateTypeAdapter implements JsonSerializer<Date>,
            JsonDeserializer<Date> {
        private final DateFormat dateFormat;

        private UTCDateTypeAdapter() {
            dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }

        @Override
        public synchronized JsonElement serialize(Date date, Type type,
                                                  JsonSerializationContext jsonSerializationContext) {
            synchronized (dateFormat) {
                String dateFormatAsString = dateFormat.format(date);
                return new JsonPrimitive(dateFormatAsString);
            }
        }

        @Override
        public synchronized Date deserialize(JsonElement jsonElement, Type type,
                                             JsonDeserializationContext jsonDeserializationContext) {
            try {
                synchronized (dateFormat) {
                    return dateFormat.parse(jsonElement.getAsString());
                }
            } catch (ParseException e) {
                throw new JsonSyntaxException(jsonElement.getAsString(), e);
            }
        }
    }
    protected final Gson gson = new GsonBuilder()
            .registerTypeAdapter(Date.class, new UTCDateTypeAdapter()) // use our timezone-specific adapter
            .create();

    private final Class<T> clazz;
    private final Map<String, String> headers;
    private final Map<String, String> body;
    private final Map<String, String> params;
    private final Response.Listener<T> listener;

    /**
     * Make a GET request and return a parsed object from JSON.
     *
     * @param url     URL of the request to make
     * @param clazz   Relevant class object, for Gson's reflection
     * @param headers Map of request headers
     */
    public GsonRequest(int method, String url, Class<T> clazz, Map<String, String> headers, Map<String, String> params, Map<String, String> body,
                       Response.Listener<T> listener, Response.ErrorListener errorListener) {

        super(method, url, null, listener, errorListener);
        this.clazz = clazz;
        this.headers = headers;
        this.params = params;
        this.body = body;
        this.listener = listener;
    }

    @Override
    public String getUrl() {
        Uri.Builder uri = Uri.parse(super.getUrl()).buildUpon();
        if (params != null) {
            for (String i : params.keySet()) {
                uri.appendQueryParameter(i, params.get(i));
            }
        }
        return uri.build().toString();
    }

    @Override
    public byte[] getBody() {
        return gson.toJson(this.body).getBytes();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null ? headers : super.getHeaders();
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(
                    gson.fromJson(json, clazz),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        if (volleyError instanceof AuthFailureError) {
            volleyError = new VolleyError("Authorisation Error");
        } else if (volleyError instanceof NetworkError) {
            if (volleyError instanceof NoConnectionError) {
                volleyError = new VolleyError("Could not connect to server");
            } else {
                volleyError = new VolleyError("Network Error");
            }
        } else if (volleyError instanceof ParseError) {
            volleyError = new VolleyError("Parse Error");
        } else if (volleyError instanceof ServerError) {
            volleyError = new VolleyError("Server Error");
        } else if (volleyError instanceof TimeoutError) {
            volleyError = new VolleyError("Timeout Error");
        }

        return volleyError;
    }
}
