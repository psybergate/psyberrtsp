package za.co.psybergate.meme.rtsplayer;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MAS {
    private static SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ArtistApplication.getAppContext());
    private static final String BASE_URL = prefs.getString("mas_api", "http://artist.dhcp.meraka.csir.co.za:8000/api/artist/4");

    // Define classes for MAS responses

    public class AID {
        String aid;

        public String getAID() {
            return aid;
        }
    }

    public class Switch {
    }

    public class Channel {
        @SerializedName("_id")
        private String id;

        @SerializedName("name")
        private String channelName;

        @SerializedName("icon")
        private String channelIcon;

        private String category;

        private String description;

        public String getId() {
            return id;
        }

        public String getChannelName() {
            return channelName;
        }
        public String getChannelDescription() {
            return description;
        }

        public Channel setChannelName(String channelName) {
            this.channelName = channelName;
            return this;
        }

        public String getChannelIcon() {
            return channelIcon;
        }

        public String getCategory() {
            return category;
        }
    }

    public class ChannelDetail {
        @SerializedName("_id")
        private String id;

        @SerializedName("name")
        private String channelName;

        @SerializedName("icon")
        private String channelIcon;

        private String category;
        private String twitter_name;
        private int uri_count;

        private String description;

        public String getId() {
            return id;
        }

        public String getChannelName() {
            return channelName;
        }

        public String getChannelIcon() {
            return channelIcon;
        }

        public String getCategory() {
            return category;
        }

        public int getUri_count() {
            return uri_count;
        }

        public String getDescription() {
            return description;
        }
    }

    public class Channels {
        List<Channel> channels;

        public List<Channel> getChannels() {
            return channels;
        }

        public void setChannels(List<Channel> channels) {
            this.channels = channels;
        }
    }

    public class URI {
        String uri;

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }
    }

    public class SendMessage {
    }

    public class ChatMessage {

        private int sequence;
        @SerializedName("_id")
        private String id;
        @SerializedName("time")
        private Date dateTime;
        @SerializedName("body")
        private String message;
        private String from;

        public String getMessage() {
            return message;
        }

        public Date getTime() {
            return dateTime;
        }

        public String getFrom() {
            return from;
        }
    }

    public class ChatMessageSeq {
        public ChatMessage doc;
        public int seq;

        public ChatMessage getDoc() {
            return doc;
        }

        public void setDoc(ChatMessage doc) {
            this.doc = doc;
        }

        public int getSeq() {
            return seq;
        }

        public void setSeq(int seq) {
            this.seq = seq;
        }
    }

    public class MonitorChatMessages {
        List<ChatMessage> results;
        String last_seq;

        public List<ChatMessage> getResults() {
            return results;
        }

        public void setResults(List<ChatMessage> results) {
            this.results = results;
        }

        public String getLast_seq() {
            return last_seq;
        }

        public void setLast_seq(String last_seq) {
            this.last_seq = last_seq;
        }
    }

    public static class GetChatMessages {

        public GetChatMessages() {
        }

        String update_seq;
        List<ChatMessage> messages;

        public String getUpdate_seq() {
            return update_seq;
        }

        public void setUpdate_seq(String update_seq) {
            this.update_seq = update_seq;
        }

        public List<ChatMessage> getMessages() {
            return messages;
        }

        public void setMessages(List<ChatMessage> messages) {
            this.messages = messages;
        }
    }

    // Uhm special class to unpack messages from the monitor call ...

    public static class MonitorMessages extends GsonRequest<GetChatMessages> {
        private static final String BASE_URL = "http://artist.dhcp.meraka.csir.co.za:8000/api/artist/4/";

        public MonitorMessages(Map<String, String> params, Response.Listener<MAS.GetChatMessages> listener, Response.ErrorListener errorListener) {
            super(Request.Method.GET, BASE_URL + "monitor", MAS.GetChatMessages.class, null, params, null, listener, errorListener);
        }

        @Override
        protected Response<MAS.GetChatMessages> parseNetworkResponse(NetworkResponse response) {
            try {
                String json = new String(
                        response.data,
                        HttpHeaderParser.parseCharset(response.headers));

                MAS.GetChatMessages msgList = new MAS.GetChatMessages();
                JSONObject jo = new JSONObject(json);

                if (jo != null) {
                    msgList.messages = new ArrayList<ChatMessage>();
                    msgList.update_seq = jo.getString("last_seq");

                    JSONArray results = jo.getJSONArray("results");
                    if (results != null) {
                        for (int i = 0; i < results.length(); ++i) {
                            JSONObject msg = results.getJSONObject(i).getJSONObject("doc");

                            if (msg != null) {
                                msgList.messages.add(0, gson.fromJson(msg.toString(), MAS.ChatMessage.class));
                            }
                        }
                    }
                }

                return Response.success(msgList, HttpHeaderParser.parseCacheHeaders(response));
            } catch (UnsupportedEncodingException e) {
                return Response.error(new ParseError(e));
            } catch (JsonSyntaxException e) {
                return Response.error(new ParseError(e));
            } catch (JSONException e) {
                return Response.error(new ParseError(e));
            }
        }
    }

    // Implement MAS interface ...

    public static GsonRequest generateID(String identifierType, String identifierValue, Response.Listener<MAS.AID> listener, Response.ErrorListener errorListener) {
        Map<String, String> body = new HashMap<String, String>();

        body.put(identifierType, identifierValue);

        GsonRequest<MAS.AID> request = new GsonRequest<MAS.AID>(Request.Method.POST, BASE_URL + "generateId", MAS.AID.class, null, null, body, listener, errorListener);
        request.setShouldCache(false);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }

    public static GsonRequest getChannelInfo(String brand, Response.Listener<MAS.Channels> listener, Response.ErrorListener errorListener) {
        Map<String, String> params = new HashMap<String, String>();

        params.put("bid", brand);

        GsonRequest<MAS.Channels> request = new GsonRequest<MAS.Channels>(Request.Method.GET, BASE_URL + "channels", MAS.Channels.class, null, params, null, listener, errorListener);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }

    public static GsonRequest getChannelDetail(String channel_id, Response.Listener<MAS.ChannelDetail> listener, Response.ErrorListener errorListener) {
        GsonRequest<MAS.ChannelDetail> request = new GsonRequest<MAS.ChannelDetail>(Request.Method.GET, BASE_URL + "channels/" + channel_id, MAS.ChannelDetail.class, null, null, null, listener, errorListener);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }

    public static GsonRequest getChannelUri(String applicationId, String channelId, String speed, Response.Listener<URI> listener, Response.ErrorListener errorListener) {
        Map<String, String> body = new HashMap<String, String>();

        if (applicationId == null) {
            applicationId = prefs.getString("AID", null);
        }

        body.put("aid", applicationId);
        body.put("cid", channelId);
        body.put("speed", speed);

        GsonRequest<MAS.URI> request = new GsonRequest<MAS.URI>(Request.Method.POST, BASE_URL + "connect", MAS.URI.class, null, null, body, listener, errorListener);
        request.setShouldCache(false);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);
        return request;
    }

    public static GsonRequest switchQuality(String applicationId, Boolean switchUp, Response.Listener<MAS.Switch> listener, Response.ErrorListener errorListener) {
        Map<String, String> body = new HashMap<String, String>();

        if (applicationId == null) {
            applicationId = prefs.getString("AID", null);
        }

        body.put("aid", applicationId);
        body.put("action", switchUp ? "up" : "down");

        GsonRequest<MAS.Switch> request = new GsonRequest<MAS.Switch>(Request.Method.POST, BASE_URL + "switch", MAS.Switch.class, null, null, body, listener, errorListener);
        request.setShouldCache(false);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }

    public static GsonRequest sendMessage(String from, String to, String message, Response.Listener<MAS.SendMessage> listener, Response.ErrorListener errorListener) {
        Map<String, String> body = new HashMap<String, String>();

        body.put("from", from);
        body.put("to", to);
        body.put("body", message);

        GsonRequest<MAS.SendMessage> request = new GsonRequest<MAS.SendMessage>(Request.Method.POST, BASE_URL + "messages", MAS.SendMessage.class, null, null, body, listener, errorListener);
        request.setShouldCache(false);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }

    public static GsonRequest getChatMessages(String channelId, Response.Listener<MAS.GetChatMessages> listener, Response.ErrorListener errorListener) {
        Map<String, String> params = new HashMap<String, String>();

        params.put("cid", channelId);

        GsonRequest<MAS.GetChatMessages> request = new GsonRequest<MAS.GetChatMessages>(Request.Method.GET, BASE_URL + "messages", MAS.GetChatMessages.class, null, params, null, listener, errorListener);

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }

    public static GsonRequest monitorChatMessages(String channelId, String since, Response.Listener<MAS.GetChatMessages> listener, Response.ErrorListener errorListener) {
        Map<String, String> params = new HashMap<String, String>();

        params.put("cid", channelId);
        params.put("since", since);

        GsonRequest request = new MonitorMessages(params, listener, errorListener);
        request.setRetryPolicy(new DefaultRetryPolicy(12000, 100, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = VolleySingleton.getInstance().getRequestQueue();
        requestQueue.add(request);

        return request;
    }
}
