package za.co.psybergate.meme.rtsplayer;

import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import java.nio.ByteBuffer;

import veg.mediaplayer.sdk.MediaPlayer;
import veg.mediaplayer.sdk.MediaPlayerConfig;

/**
 * This class echoes a string called from JavaScript.
 */
public class rtsplayer extends CordovaPlugin implements MediaPlayer.MediaPlayerCallback {


    private MediaPlayer mPlayer;
    private MediaPlayerConfig mConfig;
    private MediaPlayer.MediaPlayerCallback mediaPlayerCallback;
    private FrameLayout layout;
    private ViewGroup.LayoutParams webViewLayoutParams;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("watch")) {
            String videoSrc = args.getString(0);
            mConfig = new MediaPlayerConfig();
            mConfig.setConnectionNetworkProtocol(1);
            mConfig.setConnectionDetectionTime(2000);
            mConfig.setConnectionBufferingTime(3);
            mConfig.setDecodingType(1);
            mConfig.setRendererType(0);
            mConfig.setSynchroEnable(1);
            mConfig.setSynchroNeedDropVideoFrames(1);
            mConfig.setEnableColorVideo(1);
            mConfig.setEnableAspectRatio(1);
            mConfig.setDataReceiveTimeout(10000);
            mConfig.setNumberOfCPUCores(0);
            mConfig.setStartPreroll(0);

            mConfig.setConnectionUrl(videoSrc);
            mediaPlayerCallback = this;
            webViewLayoutParams = webView.getView().getLayoutParams();
            setMediaPlayerLayout();

            callbackContext.success();
            return true;
        } else if (action.equals("changeVideoOrientation")) {
            String currentOrientation = args.getString(0);
            setMediaPlayerSize(currentOrientation);
            callbackContext.success();
            return true;
        } else if (action.equals("getBitRateStat")) {

            CallbackContext callback = null;

            int bitRate = mPlayer.GetDataBitrateOnSource();
            Log.d("eventMediaPlayer", "bit rate: " + bitRate);

            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK, bitRate);
            pluginResult.setKeepCallback(true);
            callbackContext.sendPluginResult(pluginResult);

            return true;
        } else if (action.equals("exitPlayer")) {
            Log.d("eventMediaPlayer", "exitingPlayer...");
            mPlayer.Close();
            cordova.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    layout.removeView(mPlayer);
                    webView.getView().setLayoutParams(webViewLayoutParams);
                }
            });
            callbackContext.success();
            return true;
        }

        callbackContext.error("Rtsplayer execution error.");
        return false;

    }


    @Override
    public void onStop() {
        super.onStop();
        mPlayer.Close();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPlayer.Close();
    }


    private void setMediaPlayerLayout() {

        cordova.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                int orientation = cordova.getActivity().getResources().getConfiguration().orientation;
                layout = (FrameLayout) webView.getView().getParent();
                int mediaPlayerWidth = layout.getWidth();
                int mediaPlayerHeight;
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    mediaPlayerHeight = (int) (mediaPlayerWidth / 1.714285714);
                } else {
                    mediaPlayerHeight = layout.getHeight();
                }
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(mediaPlayerWidth, mediaPlayerHeight);
                mPlayer = new MediaPlayer(layout.getContext());
                mPlayer.setLayoutParams(params);
                layout.addView(mPlayer);
                mPlayer.Open(mConfig, rtsplayer.this.mediaPlayerCallback);
                mPlayer.setFocusable(false);
                mPlayer.clearFocus();

                params = new FrameLayout.LayoutParams(mediaPlayerWidth, layout.getHeight() - mediaPlayerHeight, Gravity.BOTTOM);
                webView.getView().setLayoutParams(params);
            }
        });
    }

    private void setMediaPlayerSize(final String currentOrientation) {
        Log.d("eventMediaPlayer", "cordova orientation: " + currentOrientation);

        cordova.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                int statusBarHeight = 0;
                int resource = cordova.getActivity().getApplicationContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
                if (resource > 0) {
                    statusBarHeight = cordova.getActivity().getApplicationContext().getResources().getDimensionPixelSize(resource);
                }

                DisplayMetrics metrics = new DisplayMetrics();
                cordova.getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
                int usableHeight = metrics.heightPixels;
                int displayHeight = usableHeight - statusBarHeight;
                int displayWidth = metrics.widthPixels;

                FrameLayout.LayoutParams videoParams;
                FrameLayout.LayoutParams messageParams;
                int mediaPlayerWidth;
                int mediaPlayerHeight;

                if (currentOrientation.equals("portrait")) {
                    mediaPlayerWidth = displayWidth;
                    mediaPlayerHeight = (int) (mediaPlayerWidth / 1.714285714);
                    messageParams = new FrameLayout.LayoutParams(displayWidth, displayHeight - mediaPlayerHeight, Gravity.BOTTOM);
                } else {
                    mediaPlayerWidth = displayWidth;
                    mediaPlayerHeight = displayHeight;
                    messageParams = new FrameLayout.LayoutParams(0, 0, Gravity.BOTTOM);
                }
                videoParams = new FrameLayout.LayoutParams(mediaPlayerWidth, mediaPlayerHeight);

                layout.updateViewLayout(mPlayer, videoParams);
                layout.updateViewLayout(webView.getView(), messageParams);

                Log.d("eventMediaPlayer", "focus view: " + ((FrameLayout) webView.getView().getParent()).getFocusedChild());
            }
        });

    }

    @Override
    public int Status(int i) {
        return 0;
    }

    @Override
    public int OnReceiveData(ByteBuffer byteBuffer, int i, long l) {
        return 0;
    }

}