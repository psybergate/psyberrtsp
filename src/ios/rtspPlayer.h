//
//  rtspPlayer.h
//  RTSP Player
//
//  Created by Recai Sinekli on 01/07/16.
//  Copyright © 2016 Recai Sinekli. All rights reserved.
//

#import <Cordova/CDVPlugin.h>
#import <Foundation/Foundation.h>
#import "M3U8.h"
#import "MediaPlayer.h"
#import "MediaPlayerConfig.h"
#import "Thumbnailer.h"
#import "ThumbnailerConfig.h"
#import "ViewController.h"

@interface rtspPlayer : CDVPlugin<MediaPlayerCallback>{}

@property (nonatomic, retain) UIView* parentView;
@property MediaPlayer *player;
@property UIActivityIndicatorView *_activityIndicatorView;
@property NSString *videoAddress;
@property UIView *frameView;

-(void) watch:(CDVInvokedUrlCommand*) command;

-(void) dismissPlayer:(CDVInvokedUrlCommand*) command;

-(void) exitPlayer:(CDVInvokedUrlCommand*) command;

-(void) getBitRateStat:(CDVInvokedUrlCommand*) command;


@property (strong,nonatomic) CDVInvokedUrlCommand* lastCommand;

@end
