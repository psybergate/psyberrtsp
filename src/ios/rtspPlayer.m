//
//  rtspPlayer.m
//  RTSP Player
//
//  Created by Recai Sinekli on 01/07/16.
//  Copyright © 2016 Recai Sinekli. All rights reserved.
//

#import "rtspPlayer.h"


@implementation rtspPlayer

@synthesize parentView, player, frameView, _activityIndicatorView;

-(void) watch:(CDVInvokedUrlCommand*) command {
    
    NSLog(@"watch recieved");
    
    self.videoAddress = [command.arguments objectAtIndex:0];
    self.parentView = nil;
    
    [UIApplication sharedApplication].idleTimerDisabled = YES;

    //make the view
    CGRect viewRect = CGRectMake(0, 0, self.webView.superview.frame.size.width, 250);
    
    self.parentView = [[UIView alloc] initWithFrame:viewRect];
    
    CGSize size = self.parentView.bounds.size;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(changeVideoSize:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"" forState:UIControlStateNormal];
    
    
    if(self.webView.superview.frame.size.width > self.webView.superview.frame.size.height) {
        //landscape
        player = [[MediaPlayer alloc] init:CGRectMake(0, 0, size.width, self.webView.superview.frame.size.height-150)];
        button.frame = CGRectMake(0, 0, size.width, self.webView.superview.frame.size.height-150);
        
    }
    else //potrait
    {
        player = [[MediaPlayer alloc] init:CGRectMake(0, 0, size.width, size.width/1.714285714)];
        button.frame = CGRectMake(0, 0, size.width, size.width/1.714285714);
    }
    self.webView.frame = CGRectMake(0, player.contentView.bounds.size.height,  self.webView.superview.frame.size.width,self.webView.superview.frame.size.height - player.contentView.bounds.size.height);
    
    _activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleWhiteLarge];
    
    _activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    
    _activityIndicatorView.center = player.contentView.center;
    
    
    [_activityIndicatorView startAnimating];
    
    
    frameView = [player contentView];
    
    
    [self.parentView addSubview:frameView];
    [self.parentView addSubview:button];
    [self.parentView addSubview:_activityIndicatorView];
    self.parentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    MediaPlayerConfig* config = [[MediaPlayerConfig alloc] init];
    config.connectionUrl = self.videoAddress;
    config.decodingType =1;
    config.numberOfCPUCores = 1;
    config.enableAudio = 1;
    [player Open:config callback:self];
    
    
    
    [self.webView.superview addSubview:self.parentView];
    
    //if(self.webView.superview.frame.size.width < self.webView.superview.frame.size.height)
    //self.webView.frame = CGRectMake(0, player.contentView.bounds.size.height,  self.webView.superview.frame.size.width,self.webView.superview.frame.size.height - size.width/1.714285714);
    
    
    self.parentView.backgroundColor = [UIColor clearColor];
    
    self.parentView.userInteractionEnabled = YES;
    
    self.webView.frame = CGRectMake(0, player.contentView.bounds.size.height,  self.webView.superview.frame.size.width,self.webView.superview.frame.size.height - player.contentView.bounds.size.height);
    [self changeVideoSize:button];
    
}

-(void) changeVideoSize:(UIButton*) overlayButton {
    CGSize size = self.parentView.bounds.size;
    if(self.webView.superview.frame.size.width > self.webView.superview.frame.size.height) {
        if (frameView.frame.size.height < self.webView.superview.frame.size.height) {
            frameView.frame = CGRectMake(0, 0, size.width, self.webView.superview.frame.size.height);
        } else {
            frameView.frame = CGRectMake(0, 0, size.width, self.webView.superview.frame.size.height-150);
        }
    }
}

-(void) getBitRateStat:(CDVInvokedUrlCommand*) command{
    CDVPluginResult* pluginResult = nil;
    
    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsInt:[player getDataBitrateOnSource]];
    
    return [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    
}


-(void) exitPlayer:(CDVInvokedUrlCommand*) command {
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;

    [self.parentView removeFromSuperview];
    
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
    
    self.webView.frame = CGRectMake(0, 0,  self.webView.superview.frame.size.width,self.webView.superview.frame.size.height);    // dismiss view from stack
    
    [player Close];
    
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                callbackId:self.lastCommand.callbackId];
}


-(void) dismissPlayer:(CDVInvokedUrlCommand*) command {
    
    [UIApplication sharedApplication].idleTimerDisabled = NO;

    [self.parentView removeFromSuperview];
    
    self.webView.frame = CGRectMake(0, 0,  self.webView.superview.frame.size.width,self.webView.superview.frame.size.height);    // dismiss view from stack*/
    
    
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK]
                                callbackId:self.lastCommand.callbackId];
}

-(int) OnReceiveData: (MediaPlayer*)player
              buffer: (void*)buffer
                size: (int) size
                 pts: (long) pts
{
    
    NSLog(@"OnReceiveData called");
    return 0;
}

- (int) OnReceiveSubtitleString: (MediaPlayer*)player
                           data: (NSString*)data
                       duration: (uint64_t)duration
{
    
    NSLog(@"OnReceiveSubtitleString called: %@, %llu", data, duration);
    return 0;
}

- (int) Status: (MediaPlayer*)player1
          args: (int)arg
{
    NSLog(@"Status called: arg code is %d for instance %@", arg, player1);
    switch(arg)
    {
        case PLP_BUILD_STARTING:
        {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [_activityIndicatorView startAnimating];
                           });
            break;
        }
        case PLP_PLAY_SUCCESSFUL:
        {
            dispatch_async(dispatch_get_main_queue(), ^
                           {
                               [_activityIndicatorView stopAnimating];
                               
                               
                           });
            break;
        }
    }
    
    return 0;
}

@end
