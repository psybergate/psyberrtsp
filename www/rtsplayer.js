var exec = require('cordova/exec');

exports.watchVideo = function(moviePath, success, error) {
    exec(success, error, "rtsplayer", "watch", [moviePath]);
};

exports.changeVideoOrientation = function(orientation, success, error) {
    exec(success, error, "rtsplayer", "changeVideoOrientation", [orientation]);
};

exports.dismissPlayer = function(success, error) {
    exec(success, error, "rtsplayer", "dismissPlayer", []);
};

exports.exitPlayer = function(success, error) {
    exec(success, error, "rtsplayer", "exitPlayer", []);
};

exports.getBitRateStat = function(success, error) {
    exec(success, error, "rtsplayer", "getBitRateStat", []);
};